package io.github.vudoku.jotnar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JotnarApplication {

	public static void main(String[] args) {
		SpringApplication.run(JotnarApplication.class, args);
	}

}
