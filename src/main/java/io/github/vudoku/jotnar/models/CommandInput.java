package io.github.vudoku.jotnar.models;

import discord4j.core.event.domain.message.MessageCreateEvent;
import org.springframework.util.StringUtils;

public record CommandInput(String command, String arg) {

  public static boolean isCommand(String msg) {
    return StringUtils.hasText(msg) && msg.startsWith("!");
  }

  public static CommandInput parse(MessageCreateEvent event) {
    return parse(event.getMessage().getContent());
  }


  public static CommandInput parse(String msg) {
    if (!isCommand(msg)) {
      throw new IllegalArgumentException("Invalid Command");
    }
    String[] split = msg.substring(1).split("\s+");
    return new CommandInput(split[0].toLowerCase(), split.length > 1 ? split[1] : null);
  }
}
