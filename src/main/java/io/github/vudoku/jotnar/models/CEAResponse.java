package io.github.vudoku.jotnar.models;

/**
 * @author VuDo
 * @since 3/10/2022
 */
public interface CEAResponse<T> {

  T data();
  String message();
}
