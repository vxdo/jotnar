package io.github.vudoku.jotnar.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;
import java.util.List;
import java.util.Map;

/**
 * @author VuDo
 * @since 3/10/2022
 */
public record UpComingResponse(
    UpComing data,
    String message
) {

  public record UpComing(
      int ace,
      String bid,
      boolean completed,
      Instant cts,
      Map<String, Object> meta,
      String mid,
      int mn,
      int rnd,
      long st,
      String tmid,
      @JsonProperty("gs") List<Game> games,
      @JsonProperty("ts") List<Team> teams
  ) {

  }

  public record Game(
      String gid,
      @JsonProperty("msg") String tournamentCode,
      int xvx
  ) {

  }

  public record Team(
      @JsonProperty("dn") String name,
      @JsonProperty("w") int win,
      @JsonProperty("l") int lose,
      @JsonProperty("t") int tie,
      @JsonProperty("r") int rank,
      String org
  ) {

  }
}
