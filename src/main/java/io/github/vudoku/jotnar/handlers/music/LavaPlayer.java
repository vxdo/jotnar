package io.github.vudoku.jotnar.handlers.music;

import static java.lang.Math.ceil;

import com.sedmelluq.discord.lavaplayer.filter.PcmFilterFactory;
import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerLifecycleManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEvent;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventListener;
import com.sedmelluq.discord.lavaplayer.player.event.TrackEndEvent;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.playback.MutableAudioFrame;
import discord4j.voice.AudioProvider;
import io.netty.util.concurrent.DefaultEventExecutor;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class LavaPlayer extends AudioProvider implements AudioLoadResultHandler {

  private final AudioPlayer player;
  private final MutableAudioFrame frame = new MutableAudioFrame();
  private final Deque<AudioTrack> queue = new ArrayDeque<>();

  private boolean isRepeat = false;

  public LavaPlayer(final AudioPlayer player) {
    // Allocate a ByteBuffer for Discord4J's AudioProvider to hold audio data for Discord
    super(ByteBuffer.allocate(StandardAudioDataFormats.DISCORD_OPUS.maximumChunkSize()));
    // Set LavaPlayer's MutableAudioFrame to use the same buffer as the one we just allocated
    frame.setBuffer(getBuffer());
    this.player = player;
    this.player.setVolume(20);
    this.player.addListener(new AudioPlayerLifecycleManager(new DefaultEventExecutor(),
        new AtomicLong(10000)));
    this.player.addListener(new PlayNextListener());
  }

  @Override
  public boolean provide() {
    // AudioPlayer writes audio data to its AudioFrame
    final boolean didProvide = player.provide(frame);
    // If audio was provided, flip from write-mode to read-mode
    if (didProvide) {
      getBuffer().flip();
    }
    return didProvide;
  }

  @Override
  public void trackLoaded(final AudioTrack track) {
    // LavaPlayer found an audio source for us to play
    Mono.just(track)
        .doOnNext(queue::add)
        .filter(t -> player.getPlayingTrack() == null && !queue.isEmpty())
        .doOnNext(o -> this.playTrack())
        .subscribeOn(Schedulers.boundedElastic())
        .subscribe();
  }

  @Override
  public void playlistLoaded(final AudioPlaylist playlist) {
    // LavaPlayer found multiple AudioTracks from some playlist
    Mono.just(playlist)
        .doOnNext(l -> queue.addAll(l.getTracks()))
        .filter(t -> player.getPlayingTrack() == null && !queue.isEmpty())
        .doOnNext(o -> this.playTrack())
        .subscribeOn(Schedulers.boundedElastic())
        .subscribe();
  }

  @Override
  public void noMatches() {
    // LavaPlayer did not find any audio to extract
  }

  @Override
  public void loadFailed(final FriendlyException exception) {
    // LavaPlayer could not parse an audio source for some reason
  }

  public void skip() {
    if (queue.size() == 0) {
      player.stopTrack();
    }
    startTrack(poll(), false);
  }

  public boolean repeat() {
    this.isRepeat = !this.isRepeat;
    return this.isRepeat;
  }

  public String printQueue(int page) {

    List<AudioTrack> tracks = List.copyOf(queue);
    StringBuilder builder = new StringBuilder();
    builder.append("Currently playing: ").append(player.getPlayingTrack().getInfo().title)
        .append("\n");
    builder.append("Queue: ");
    if (queue.isEmpty()) {
      builder.append("Empty");
    }
    builder.append("\n");
    for (int i = (page - 1) * 15; i < (page - 1) * 15 + 15; i++) {
      if (i == queue.size()) {
        break;
      }
      builder.append(" ").append(i + 1).append(". ").append(tracks.get(i).getInfo().title)
          .append("\n");
    }
    int currentPage = (int) ceil(page * 1.0 / 15);
    int totalPage = (int) ceil(queue.size() * 1.0 / 15);
    builder.append("Page ").append(currentPage).append('/').append(totalPage > 0 ? totalPage : 1);
    return builder.toString();
  }

  public void clear() {
    this.queue.clear();
  }


  public AudioTrack getPlayingTrack() {
    return player.getPlayingTrack();
  }

  public void playTrack() {
    player.startTrack(poll(), true);
  }

  public void playTrack(int index) {
    if (player.getPlayingTrack() == null && !queue.isEmpty()) {
      return;
    }
    index = Math.max(index, 1);
    AudioTrack track = new ArrayList<>(queue).get(index - 1);
    queue.remove(track);
    player.playTrack(track);
  }

  public void playTrack(AudioTrack track) {
    player.playTrack(track);
  }

  public boolean startTrack(AudioTrack track, boolean noInterrupt) {
    return player.startTrack(track, noInterrupt);
  }

  public void stop() {
    stopTrack();
    clear();
  }

  public void stopTrack() {
    player.stopTrack();
  }

  public int getVolume() {
    return player.getVolume();
  }

  public void setVolume(int volume) {
    player.setVolume(volume);
  }

  public void setFilterFactory(PcmFilterFactory factory) {
    player.setFilterFactory(factory);
  }

  public void setFrameBufferDuration(Integer duration) {
    player.setFrameBufferDuration(duration);
  }

  public boolean isPaused() {
    return player.isPaused();
  }

  public void setPaused(boolean value) {
    player.setPaused(value);
  }

  public void destroy() {
    player.destroy();
  }

  public void addListener(AudioEventListener listener) {
    player.addListener(listener);
  }

  public void removeListener(AudioEventListener listener) {
    player.removeListener(listener);
  }

  public void checkCleanup(long threshold) {
    player.checkCleanup(threshold);
  }

  private AudioTrack poll() {
    AudioTrack track = queue.poll();
    // Shouldn't have to worry about null because poll is only called when adding track(s)
    if (isRepeat) {
      queue.add(track);
    }
    return track;
  }

  private class PlayNextListener implements AudioEventListener {

    @Override
    public void onEvent(AudioEvent event) {
      Mono.just(event)
          .filter(t -> player.getPlayingTrack() == null && !queue.isEmpty())
          .cast(TrackEndEvent.class)
          .doOnNext(e -> player.playTrack(queue.poll()))
          .subscribe();
    }
  }
}
