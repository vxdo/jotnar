package io.github.vudoku.jotnar.handlers.music;

import discord4j.core.event.domain.VoiceStateUpdateEvent;
import discord4j.core.object.VoiceState;
import io.github.vudoku.jotnar.configs.DiscordClient;
import io.github.vudoku.jotnar.handlers.AbstractHandler;
import java.util.Optional;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class EmptyVoiceChannelDetector extends AbstractHandler<VoiceStateUpdateEvent> {

  private final MusicCache cache;

  public EmptyVoiceChannelDetector(DiscordClient discordClient, MusicCache cache) {
    super(discordClient);
    this.cache = cache;
  }

  @PostConstruct
  public void register() {
    discordClient.registerVoiceStateUpdateHandler(this);
  }

  @Override
  public Mono<Void> handle(VoiceStateUpdateEvent event) {
    return Mono.just(event)
        .map(VoiceStateUpdateEvent::getOld)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .flatMap(VoiceState::getChannel)
        .flatMap(c -> c.getVoiceStates().count())
        .filter(count -> count <= 2 && event.getOld().isPresent())
        .flatMap(count -> Mono.just(event.getOld().get().getChannelId())
            .filter(Optional::isPresent)
            .map(Optional::get)
            .flatMap(cache::remove))
        .then();

  }
}
