package io.github.vudoku.jotnar.handlers.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.VoiceChannel;
import discord4j.core.spec.VoiceChannelJoinSpec;
import discord4j.voice.VoiceConnection;
import io.github.vudoku.jotnar.configs.DiscordClient;
import io.github.vudoku.jotnar.handlers.AbstractHandler;
import io.github.vudoku.jotnar.models.CommandInput;
import javax.annotation.PostConstruct;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@EqualsAndHashCode(callSuper = false)
public class MusicHandler extends AbstractHandler<MessageCreateEvent> {

  private final AudioPlayerManager playerManager;
  private final LavaPlayer player;
  private final MusicCache cache;

  public MusicHandler(DiscordClient discordClient,
      AudioPlayerManager playerManager,
      LavaPlayer player,
      MusicCache cache) {
    super(discordClient);
    this.cache = cache;
    this.playerManager = playerManager;
    this.player = player;
  }

  @PostConstruct
  public void register() {
    discordClient.registerMessageCreateHandler(this);
  }

  @Override
  public Mono<Void> handle(MessageCreateEvent event) {
    CommandInput input = CommandInput.parse(event);
    switch (input.command()) {
      case "play":
      case "p":
        return join(event)
            .flatMap(o -> play(event, input));
      case "volume":
      case "vol":
      case "v":
        try {
          return setVolume(event, Math.abs(Integer.parseInt(input.arg())));
        } catch (NumberFormatException e) {
          return handleError(event, "Volume must be a number");
        }
      case "pause":
        return keepPlaying(event, false);
      case "resume":
        return keepPlaying(event, true);
      case "list":
      case "ls":
      case "l":
        try {
          return listQueue(event, Math.abs(Integer.parseInt(input.arg())));
        } catch (NumberFormatException | NullPointerException e) {
          return listQueue(event, 1);
        }
      case "repeat":
      case "r":
        return repeat(event);
      case "clear":
      case "cls":
        return clear(event);
      case "stop":
        return stop(event);
      case "quit":
      case "q":
        return quit(event);
      case "skip":
      case "next":
        return next(event);
      default:
        return Mono.empty();
    }
  }

  @Override
  public Mono<Void> handleError(MessageCreateEvent event, String msg) {
    String err = ERROR_MSG.formatted(event.getMember().get().getId().asLong()).concat(msg);
    return event.getMessage().getChannel()
        .flatMap(channel -> channel.createMessage(err))
        .then();
  }

  private Mono<Void> quit(MessageCreateEvent event) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .doOnNext(o -> player.stop())
        .flatMap(cache::remove);
  }

  private Mono<Void> next(MessageCreateEvent event) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .doOnNext(LavaPlayer::skip)
        .then();
  }

  private Mono<Void> stop(MessageCreateEvent event) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .doOnNext(LavaPlayer::stop)
        .then();
  }

  private Mono<Void> clear(MessageCreateEvent event) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .doOnNext(LavaPlayer::clear)
        .then();
  }

  private Mono<Void> repeat(MessageCreateEvent event) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .map(LavaPlayer::repeat)
        .then();
  }

  private Mono<Void> listQueue(MessageCreateEvent event, int page) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .map(player -> player.printQueue(page))
        .switchIfEmpty(Mono.just("Queue is empty"))
        .zipWith(event.getMessage().getChannel())
        .flatMap(tuple -> tuple.getT2().createMessage(tuple.getT1()))
        .then();
  }

  private Mono<Void> keepPlaying(MessageCreateEvent event, boolean keepPlaying) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .doOnNext(player -> player.setPaused(!keepPlaying))
        .then();
  }

  private Mono<Void> setVolume(MessageCreateEvent event, int volume) {
    return Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .flatMap(cache::getPlayer)
        .doOnNext(player -> player.setVolume(volume))
        .then();
  }

  private Mono<Void> play(MessageCreateEvent event, CommandInput input) {
    Mono<Snowflake> channelIdCache = Mono.justOrEmpty(event.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        .map(VoiceChannel::getId)
        .cache();
    return channelIdCache
        .zipWith(channelIdCache.flatMap(cache::getPlayer))
        .doOnNext(tuple -> playerManager.loadItem(input.arg(), tuple.getT2()))
        .flatMap(tuple -> cache.getPlayer(tuple.getT1()))
        .doOnNext(player -> {
          if (StringUtils.hasLength(input.arg())
              && input.arg().chars().allMatch(Character::isDigit)) {
            player.playTrack(Math.abs(Integer.parseInt(input.arg())));
          }
        })
        .then();
  }

  private Mono<VoiceConnection> join(MessageCreateEvent e) {
    return Mono.justOrEmpty(e.getMember())
        .flatMap(Member::getVoiceState)
        .flatMap(VoiceState::getChannel)
        // join returns a VoiceConnection which would be required if we were
        // adding disconnection features, but for now we are just ignoring it.
        .flatMap(channel -> Mono.zip(
            Mono.just(channel),
            cache.putPlayer(channel.getId(), new LavaPlayer(playerManager.createPlayer()))
        ))
        .flatMap(tuple -> tuple.getT1().join(VoiceChannelJoinSpec.builder()
            .selfDeaf(true)
            .provider(tuple.getT2())
            .build()))
        .flatMap(cache::putConnection);
  }
}
