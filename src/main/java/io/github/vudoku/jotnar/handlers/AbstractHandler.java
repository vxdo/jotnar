package io.github.vudoku.jotnar.handlers;

import discord4j.core.event.domain.Event;
import io.github.vudoku.jotnar.configs.DiscordClient;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @author VuDo
 * @since 3/10/2022
 */
@Slf4j
public abstract class AbstractHandler<T extends Event> {

  protected static final String ERROR_MSG = "<@%d> ";

  protected final DiscordClient discordClient;

  public AbstractHandler(DiscordClient discordClient) {
    this.discordClient = discordClient;
  }

  public abstract Mono<Void> handle(T event);

  public Mono<Void> handleError(T event, String msg) {
    return Mono.empty();
  }

  public Mono<Void> handleError(Throwable error) {
    log.error("Unable to process event", error);
    return Mono.empty();
  }
}
