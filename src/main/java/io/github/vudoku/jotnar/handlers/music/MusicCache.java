package io.github.vudoku.jotnar.handlers.music;

import static java.util.function.Predicate.not;

import discord4j.common.util.Snowflake;
import discord4j.voice.VoiceConnection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import reactor.core.publisher.Mono;

public class MusicCache {

  private final Map<Snowflake, VoiceConnection> connections = new ConcurrentHashMap<>();
  private final Map<Snowflake, LavaPlayer> players = new ConcurrentHashMap<>();

  public Mono<LavaPlayer> getPlayer(Snowflake snowflake) {
    return Mono.just(snowflake)
        .map(flake -> Optional.ofNullable(players.get(flake)))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .switchIfEmpty(Mono.empty());
  }

  public Mono<LavaPlayer> putPlayer(Snowflake snowflake, LavaPlayer audioPlayer) {
    return Mono.just(snowflake)
        .filter(not(players::containsKey))
        .doOnNext(flake -> players.put(flake, audioPlayer))
        .thenReturn(audioPlayer);
  }

  public Mono<VoiceConnection> putConnection(VoiceConnection voiceConnection) {
    return voiceConnection.getChannelId()
        .filter(not(connections::containsKey))
        .doOnNext(flake -> connections.put(flake, voiceConnection))
        .thenReturn(voiceConnection);
  }

  public Mono<Void> remove(Snowflake flake) {
    LavaPlayer player = players.remove(flake);
    if (player != null) {
      player.destroy();
    }
    try {
      return connections.remove(flake).disconnect();
    } catch (Exception e) {
      return Mono.empty();
    }
  }
}
