package io.github.vudoku.jotnar.configs;

import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.VoiceStateUpdateEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.gateway.intent.IntentSet;
import io.github.vudoku.jotnar.handlers.AbstractHandler;
import io.github.vudoku.jotnar.models.CommandInput;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * @author VuDo
 * @since 3/9/2022
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
public class DiscordClient {

  private final List<AbstractHandler<MessageCreateEvent>> messageCreateHandlers;
  private final List<AbstractHandler<VoiceStateUpdateEvent>> voiceStateUpdateHandlers;

  public DiscordClient(@Value("${discord.token}") String token) {
    this.messageCreateHandlers = new ArrayList<>();
    this.voiceStateUpdateHandlers = new ArrayList<>();
    GatewayDiscordClient gatewayDiscordClient = DiscordClientBuilder.create(token)
        .build()
        .gateway()
        .setEnabledIntents(IntentSet.all())
        .login()
        .block();
    gatewayDiscordClient.on(MessageCreateEvent.class)
        .flatMap(event -> Mono.just(event.getMessage().getContent())
            .filter(CommandInput::isCommand)
            .flatMap(content -> Flux.fromIterable(messageCreateHandlers)
                .flatMap(handler -> handler.handle(event))
                .onErrorContinue((err, obj) -> event.getMessage().getChannel()
                    .flatMap(channel -> {
                      log.error(err.getMessage(), err);
                      return channel.createMessage(err.getMessage());
                    })
                    .subscribeOn(Schedulers.boundedElastic())
                    .subscribe())
                .next()))
        .then()
        .subscribe();
    gatewayDiscordClient.on(VoiceStateUpdateEvent.class)
        .flatMap(event -> Flux.fromIterable(voiceStateUpdateHandlers)
            .flatMap(handler -> handler.handle(event))
            .next())
        .then()
        .subscribe();
  }

  public void registerMessageCreateHandler(AbstractHandler<MessageCreateEvent> handler) {
    this.messageCreateHandlers.add(handler);
  }

  public void registerVoiceStateUpdateHandler(AbstractHandler<VoiceStateUpdateEvent> handler) {
    this.voiceStateUpdateHandlers.add(handler);
  }
}
