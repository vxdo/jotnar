package io.github.vudoku.jotnar.configs;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer;
import io.github.vudoku.jotnar.handlers.music.MusicCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class MusicConfig {

  @Bean
  public AudioPlayerManager playerManager() {
    AudioPlayerManager playerManager = new DefaultAudioPlayerManager();
    playerManager.getConfiguration().setFrameBufferFactory(NonAllocatingAudioFrameBuffer::new);
    AudioSourceManagers.registerRemoteSources(playerManager);
    return playerManager;
  }

  @Bean
  public AudioPlayer audioPlayer(AudioPlayerManager playerManager) {
    return playerManager.createPlayer();
  }

  @Bean
  public MusicCache joinedVoiceChannels() {
    return new MusicCache();
  }
}
