package io.github.vudoku.jotnar.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author VuDo
 * @since 3/10/2022
 */
@Configuration(proxyBeanMethods = false)
public class WebClientConfig {

  @Bean
  public WebClient webClient() {
    return WebClient.builder()
        .baseUrl("https://1ebv8yx4pa.execute-api.us-east-1.amazonaws.com/prod/tournaments/fDHMUmljd7")
        .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
        .build();
  }
}
